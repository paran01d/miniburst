set :application, "miniburst"
set :domain, "mankies.com"
set :deploy_to,   "/var/www/#{application}"
set :app_path,    "app"

set :user, "deploy"
set :branch, "master"
set :group, "deploy"
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "id_dsa")]

set :symfony_env_prod, "dev"
set :shared_children,     [app_path + "/logs", web_path + "/uploads", "vendor"]
set :use_composer, true
set :vendors_mode, "install"


set :repository,  "git@bitbucket.org:paran01d/miniburst.git"
set :git_enable_submodules, 1
set :scm,         :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `subversion`, `mercurial`, `perforce`, or `none`

set :model_manager, "doctrine"
# Or: `propel`

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server

set :webserver_user,    "www-data"

logger.level = Logger::MAX_LEVEL

default_run_options[:pty] = true
set :deploy_via, :remote_cache
set :use_sudo, false
set :copy_exclude, [".git", ".DS_Store", ".gitignore", ".gitmodules"]

set  :keep_releases,  3

# Change ACL on the app/logs and app/cache directories
after 'deploy', 'deploy:update_acl'

# This is a custom task to set the ACL on the app/logs and app/cache directories
namespace :deploy do

  task :update_acl, :roles => :app do
    shared_dirs = [
      app_path + "/logs",
      app_path + "/cache"
    ]
    
    # Allow directories to be writable by webserver and this user
    run "cd #{latest_release} && #{sudo} setfacl -R -m u:www-data:rwx -m u:#{webserver_user}:rwx #{shared_dirs.join(' ')}"
    run "cd #{latest_release} && #{sudo} setfacl -dR -m u:www-data:rwx -m u:#{webserver_user}:rwx #{shared_dirs.join(' ')}"
  end
end
