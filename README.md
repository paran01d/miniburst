MiniBurst
========================

Welcome to MiniBurst - a fully-functional Symfony2
application that you can use to send an sms through a RESTFul API.

1) Installing the Standard Edition
----------------------------------

When it comes to installing MiniBurst, you have the
following options.

### Use Composer (*recommended*)

Download composer (see above) and run the
following command:

    php composer.phar install

2) Checking your System Configuration
-------------------------------------

Before starting coding, make sure that your local system is properly
configured for Symfony.

Execute the `check.php` script from the command line:

    php app/check.php

The script returns a status code of `0` if all mandatory requirements are met,
`1` otherwise.

Access the `config.php` script from a browser:

    http://localhost/path/to/symfony/app/web/config.php

If you get any warnings or recommendations, fix them before moving on.

3) Browsing the Demo Application
--------------------------------

Congratulations! You're now ready to use MiniBurst.

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!
