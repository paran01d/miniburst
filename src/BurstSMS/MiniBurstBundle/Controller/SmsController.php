<?php

namespace BurstSMS\MiniBurstBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\View,
    FOS\RestBundle\Controller\Annotations\QueryParam;

/**
 * Class: SmsController
 *
 * @see FOSRestController
 */
class SmsController extends FOSRestController {

    /**
     * getSMSSendAction - send an sms via kannel
     *
     * @param ParamFetcher $paramFetcher
     * @param string $num phone number to send sms too
     * @param string $msg msg to send
     *
     * @QueryParam(name="num", requirements="\d+", description="Phone to send SMS to.")
     * @QueryParam(name="msg", description="SMS Msg to send.")
     *
     * @View()
     */
    public function getSmsSendAction(ParamFetcher $paramFetcher) {

        $num = $paramFetcher->get('num');
        $msg = $paramFetcher->get('msg');

        $username = $this->container->getParameter("burst_sms_mini_burst.kannel_username");
        $password = $this->container->getParameter("burst_sms_mini_burst.kannel_password");
        $msc = $this->container->getParameter("burst_sms_mini_burst.kannel_msc");

        $client = $this->get('kannel.client');

        $response = $client->get('sendsms', array(), array(
            'query' => array(
            'username' => $username,
            'password'=> $password,
            'to'=>$num,
            'text'=>$msg, 
            'msc'=>$msc))
        )->send();

        $outcome = $this->parseKannelResponse($response);

        $data = array('response' => $outcome);

        return $data;
    }

    /**
     * parseKannelResponse
     *
     * @param mixed $body
     */
    private function parseKannelResponse($response) {

        $outcome = 'unknown outcome';

        if ( $response->getStatusCode() == 202 ) {
            $matches = array();
            $body = (string)$response->getBody();
            if ( preg_match('/^(?P<code>.*): (?P<msg>.*)$/', $body, $matches ) ) {
                if ( $matches['code'] == 0 ) {
                    $outcome = 'Message has been accepted for delivery';
                } elseif ( $matches['code'] == 3 ) {
                    $outcome = 'Message has been queued for later delivery';
                }
            }
        } else {
            throw \Exception('Error communicating with the SMS server');
        }

        return $outcome;
    }
}

?>
