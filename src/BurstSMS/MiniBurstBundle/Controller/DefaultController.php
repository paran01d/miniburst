<?php

namespace BurstSMS\MiniBurstBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BurstSMSMiniBurstBundle:Default:index.html.twig', array('name' => $name));
    }
}
