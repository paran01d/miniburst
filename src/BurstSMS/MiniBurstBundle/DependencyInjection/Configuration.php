<?php

namespace BurstSMS\MiniBurstBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('burst_sms_mini_burst');

        $rootNode
            ->children()
            ->scalarNode('kannel_username')->end()
            ->scalarNode('kannel_password')->end()
            ->scalarNode('kannel_msc')->defaultValue('vianett')->end()
        ->end();

        return $treeBuilder;
    }
}
