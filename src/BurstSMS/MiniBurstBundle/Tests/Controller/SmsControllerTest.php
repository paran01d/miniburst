<?php

namespace BurstSMS\MiniBurstBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SmsControllerTest extends WebTestCase
{

    public function testIndex()
    {
        $client = static::createClient();
        
        $client->request('GET', '/sms/send?num=123456789&msg=testmessage');

        $response = $client->getResponse();

        $this->assertJsonResponse($response, 200);
        

    }

    protected function assertJsonResponse($response, $statusCode = 200)
    {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(),
            $response->getContent()
        );
        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            $response->headers
        );
    }
}
